package ru.inno.homeworklesson14;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ThreadPoolTest {
    private ThreadPool threadPool;

    @BeforeEach
    void setUp() {
        threadPool = new ThreadPool();
    }

    @Test
    void createPools() throws InterruptedException {
        StringBuilder testResult = new StringBuilder();
        testResult.append("Are Petersburg beautiful.\n");
        testResult.append("Petersburg the Great city.\n");
        String[] sources = {"src/ru/inno/homeworklesson14/testresources/test.txt"};
        String[] words = {"Petersburg"};
        StringBuilder builder = new StringBuilder();
        int pools = 10;
        threadPool.createPools(sources, words, builder, pools);
        assertEquals(testResult.toString(), builder.toString());
        int wrongpools = 0;
        assertThrows(IllegalArgumentException.class, () -> threadPool.createPools(sources, words, builder, wrongpools));
    }

}