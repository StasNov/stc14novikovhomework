package ru.inno.homeworklesson14;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class WordFinderCallTest {

    @Test
    void getOccurencies() {
        StringBuilder testResult = new StringBuilder();
        testResult.append("Are Petersburg beautiful.\n");
        testResult.append("Petersburg the Great city.\n");
        String[] sourses = {"src/ru/inno/homeworklesson14/testresources/test.txt"};
        String[] words = {"Petersburg"};
        String res = "src/ru/inno/homeworklesson14/testresults/testResult.txt";
        int pools = 10;
        WordFinderCall finderCall = new WordFinderCall(pools);
        finderCall.getOccurencies(sourses, words, res);
        assertEquals(testResult.toString(), finderCall.getResultString().toString());
    }

    @Test
    void getOccurenciesException() {
        int wrongpools = 0;
        WordFinderCall finderCallWrong = new WordFinderCall(wrongpools);
        String[] sourses = {"src/ru/inno/homeworklesson14/testresources/test.txt"};
        String[] words = {"Petersburg"};
        String res = "src/ru/inno/homeworklesson14/testresults/testResult.txt";
        assertThrows(IllegalArgumentException.class, () -> finderCallWrong.getOccurencies(sourses, words, res));
    }
}