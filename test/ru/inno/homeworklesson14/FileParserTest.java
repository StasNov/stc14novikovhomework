package ru.inno.homeworklesson14;

import org.junit.jupiter.api.Test;

import java.nio.file.FileSystemNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FileParserTest {

    @Test
    void findRun() {
        List<String> testResult = new ArrayList<>();
        testResult.add("Are Petersburg beautiful.\n");
        testResult.add("Petersburg the Great city.\n");
        List<String> result = new ArrayList<>();
        String sourse = "src/ru/inno/homeworklesson14/testresources/test.txt";
        String[] words = {"Petersburg"};
        FileParser parser = new FileParser(result, sourse, words);
        parser.find(false);
        assertEquals(testResult, result);
    }

    @Test
    void findCall() {
        StringBuilder testResult = new StringBuilder();
        testResult.append("Are Petersburg beautiful.\n");
        testResult.append("Petersburg the Great city.\n");
        StringBuilder result = new StringBuilder();
        String sourse = "src/ru/inno/homeworklesson14/testresources/test.txt";
        String[] words = {"Petersburg"};
        FileParser parser = new FileParser(sourse, words, result);
        parser.find(true);
        assertEquals(testResult.toString(), result.toString());
    }

    @Test
    void findException() {
        StringBuilder result = new StringBuilder();
        String sourse = "test.txt";
        String[] words = {"Petersburg"};
        FileParser parser = new FileParser(sourse, words, result);
        assertThrows(FileSystemNotFoundException.class, () -> parser.find(true));
    }
}