package ru.inno.homeworklesson14;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WordFinderRunTest {
    private WordFinderRun finderRun;

    @BeforeEach
    void setUp() {
        finderRun = new WordFinderRun();
    }

    @Test
    void getOccurencies() {
        List<String> testResult = new ArrayList<>();
        testResult.add("Are Petersburg beautiful.\n");
        testResult.add("Petersburg the Great city.\n");
        String[] sourses = {"src/ru/inno/homeworklesson14/testresources/test.txt"};
        String[] words = {"Petersburg"};
        String res = "src/ru/inno/homeworklesson14/testresults/testResult.txt";
        finderRun.getOccurencies(sourses, words, res);
        assertEquals(testResult, finderRun.getResult());
    }
}