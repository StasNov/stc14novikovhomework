package ru.inno.homeworklesson14;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FileInFolderTest {
    private FileInFolder files;

    @BeforeEach
    void setUp() {
        files = new FileInFolder();
    }

    @Test
    void getFilesFromFolder() {
        String path = "src/ru/inno/homeworklesson14/testresources/";
        String result = "src\\ru\\inno\\homeworklesson14\\testresources\\test.txt";
        assertEquals(result, files.getFilesFromFolder(path)[0]);
    }

    @Test
    void getFilesFromFolderException() {
        String path = "somefile.txt";
        assertThrows(NullPointerException.class, () -> files.getFilesFromFolder(path));
    }
}