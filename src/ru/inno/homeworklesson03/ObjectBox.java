package ru.inno.homeworklesson03;

import java.util.Collection;
import java.util.Objects;

/**
 * Класс ObjectBox. Хранит коллекцию Object.
 */
public class ObjectBox {
    protected Collection<Object> elemCollection;

    public ObjectBox(Collection<Object> collection) {
        this.elemCollection = collection;
    }

    /**
     * Добавление объекта
     *
     * @param o - добавляемый объект
     * @return - резульат выполения команды добавления - true/false
     */
    public boolean addObject(Object o) {
        return elemCollection.add(o);
    }

    /**
     * Удаление объекта
     *
     * @param o - удаляемый объект
     * @return - резульат выполения команды удаления - true/false
     */
    public boolean deleteObject(Object o) {
        return elemCollection.remove(o);
    }

    /**
     * Вывод содержимого коллекции в строку
     */
    public void dump() {
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "Box{" +
                "elemSet=" + elemCollection +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(elemCollection);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ObjectBox box = (ObjectBox) obj;
        return elemCollection.equals(box.elemCollection);
    }
}
