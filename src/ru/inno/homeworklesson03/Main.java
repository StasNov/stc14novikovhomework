package ru.inno.homeworklesson03;

public class Main {
    public static void main(String[] args) {
        Integer[] arr = {15, 16, 4, 8, 42, 23};
        MathBox<Integer> box = new MathBox<>(arr);
        System.out.println(box);
        System.out.println(box.summator());
        System.out.println(box.splitter(2));
        System.out.println("Демонстрация деления на 0");
        try {
            box.splitter(0);
        } catch (IllegalArgumentException ex){
            ex.printStackTrace();
        }
        System.out.println("Удаляем элемент - 8");
        box.deleteElem(8);
        System.out.println(box);
        System.out.println("Добавляем элемент - 295");
        box.addObject(295);
        System.out.println(box);
        System.out.println("Демонстрация невозмжности положить объект в box");
        try {
            box.addObject("Hello");
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
    }
}