package ru.inno.homeworklesson03;

import java.util.TreeSet;

/**
 * Класс MathBox. Работает с любым Number.
 */
public class MathBox<T extends Number> extends ObjectBox {

    /**
     * Конструктор.
     * Добавление элементов массива в коллекцию.
     *
     * @param arr - массив объектов Number или наследников Number.
     */
    public MathBox(T[] arr) {
        super(new TreeSet<>());
        for (T elem : arr) {
            elemCollection.add(elem);
        }
    }

    /**
     * Подсчет суммы всех элементов коллекции
     */
    public double summator() {
        double sum = 0;
        for (Object elem : elemCollection) {
            sum += ((T) elem).doubleValue();
        }
        return sum;
    }

    /**
     * Поочередное деление всех хранящихся в объекте элементов на делитель, являющийся аргументом метода
     *
     * @param divider - делитель
     * @return коллекция с результатами деления. Коллекция внутри mathbox при этом не меняется
     */
    public TreeSet<Double> splitter(double divider) {
        if (divider == 0) {
            throw new IllegalArgumentException("Argument 'divider' is 0");
        }
        TreeSet<Double> elems = new TreeSet<>();
        for (Object elem : elemCollection) {
            elems.add(((T) elem).doubleValue() / divider);
        }
        return elems;
    }

    /**
     * Удаление элемента коллекции, в случае если он в ней содержится
     *
     * @param elem - удаляемый элемент
     * @return результат выполнения операции удаления.
     * В случае наличия в коллекции элемента и его удаления - true,
     * в случае отсутствия в коллекции элемента - false
     */
    public boolean deleteElem(T elem) {
        return elemCollection.remove(elem);
    }

    /**
     * Добавление в коллекцию элемента.
     *
     * @param o - должен быть Number или его наследником, в противном случае вызывается исключение
     * @return - резульат выполения команды добавления - true/false
     */
    @Override
    public boolean addObject(Object o) {
        if (o instanceof Number) {
            return super.addObject(o);
        } else {
            throw new IllegalArgumentException("Argument is not Number");
        }
    }
}
