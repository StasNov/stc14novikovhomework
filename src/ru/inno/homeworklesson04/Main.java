package ru.inno.homeworklesson04;


public class Main {
    public static void main(String[] args) {
        FileGenerator fileGenerator = new FileGenerator();
        String path = "src/ru/inno/homeworklesson04/";
        String[] words = {"Mercedes", "BMW", "Volkswagen", "Volvo", "Toyota", "Hummer", "Honda", "Ferrari", "Porsche"};
        fileGenerator.getFiles(path, 3, 10, words, 5);
    }
}
