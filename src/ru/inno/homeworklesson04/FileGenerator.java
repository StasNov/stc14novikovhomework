package ru.inno.homeworklesson04;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 * Генератор текстовых файлов и их содержимого
 */
public class FileGenerator {

    /**
     * Создание файлов и запись в них содержимого
     *
     * @param path        Каталог в котором будут создаваться файлы
     * @param n           Количество файлов
     * @param size        Количество абзацев в тексте файла
     * @param words       Массив слов
     * @param probability Вероятность вхождения одного из слов массива words в следующее предложение
     */
    public void getFiles(String path, int n, int size, String[] words, int probability) {
        for (int i = 0; i < n; i++) {
            try (FileWriter fileWriter = new FileWriter(path + "File" + (i + 1) + ".txt")) {
                String textForFile = createText(size, probability, words);
                fileWriter.write(textForFile);
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    /**
     * Создание слова на основе букв английского алфавита
     *
     * @return Слово
     */
    private String createWord() {
        String word = "";
        int maxCountLettersInWord = 15;
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        Random rnd = new Random();
        int lengthOfWord = 1 + rnd.nextInt(maxCountLettersInWord);
        for (int i = 0; i < lengthOfWord; i++) {
            word += alphabet.charAt(rnd.nextInt(alphabet.length()));
        }
        return word;
    }

    /**
     * Создание предложения
     *
     * @param probability Вероятность вхождения одного из слов массива words в следующее предложение
     * @param words       Массив слов
     * @return Предложение содержащее слова
     */
    private String createSentence(int probability, String[] words) {
        String sentence = "";
        int maxCountWordsInSentence = 15;
        String[] spaceBetweenWords = {" ", ", "};
        String[] signsOfEnd = {".", "!", "?"};
        Random rnd = new Random();
        int lengthOfSentence = 1 + rnd.nextInt(maxCountWordsInSentence);
        for (int i = 0; i < lengthOfSentence; i++) {
            sentence += createWord() + spaceBetweenWords[rnd.nextInt(spaceBetweenWords.length)];
        }
        if (sentence.charAt(sentence.length() - 2) == ',') {
            sentence = sentence.substring(0, sentence.length() - 2);
        } else {
            sentence = sentence.substring(0, sentence.length() - 1);
        }
        if (checkProbability(probability)) {
            sentence = replaceWord(sentence, words);
        }
        String signOfEnd = signsOfEnd[rnd.nextInt(signsOfEnd.length)];
        return sentence.substring(0, 1).toUpperCase() + sentence.substring(1) + signOfEnd;
    }

    /**
     * Создание абзаца
     *
     * @param probability Вероятность вхождения одного из слов массива words в следующее предложение
     * @param words       Массив слов
     * @return Абзац, содержащий предложения
     */
    private String createParagraph(int probability, String[] words) {
        String paragraph = "";
        int maxCountSentencesInParagraph = 20;
        Random rnd = new Random();
        int lengthOfParagraph = 1 + rnd.nextInt(maxCountSentencesInParagraph);
        for (int i = 0; i < lengthOfParagraph; i++) {
            paragraph += createSentence(probability, words) + " ";
        }
        return paragraph + "\r\n";
    }

    /**
     * Создание текста
     *
     * @param size        Количество абзацев
     * @param probability Вероятность вхождения одного из слов массива words в следующее предложение
     * @param words       Массив слов
     * @return Текст, содержащий абзацы.
     */
    private String createText(int size, int probability, String[] words) {
        String text = "";
        for (int i = 0; i < size; i++) {
            text += createParagraph(probability, words);
        }
        return text;
    }

    /**
     * Проверка соответствующей вероятности
     *
     * @param probability Вероятность
     * @return Результат проверки соотвествия (true/false)
     */
    private boolean checkProbability(int probability) {
        Random rnd = new Random();
        return rnd.nextDouble() <= 1. / probability;
    }

    /**
     * Замена случайного слова в сформированном предложении на случайное слово из массива words
     *
     * @param sentence Предложение в котором будет производиться замена случайного слова
     * @param words    Массив слов
     * @return Предложение с замененным случайным словом
     */
    private String replaceWord(String sentence, String[] words) {
        Random rnd = new Random();
        String[] wordsFromSentences = sentence.split(" ");
        wordsFromSentences[rnd.nextInt(wordsFromSentences.length)] = words[rnd.nextInt(words.length)];
        StringBuilder stringBuilder = new StringBuilder();
        for (String word : wordsFromSentences) {
            stringBuilder.append(word).append(" ");
        }
        String result = stringBuilder.toString();
        return result.substring(0, result.length() - 1);
    }
}
