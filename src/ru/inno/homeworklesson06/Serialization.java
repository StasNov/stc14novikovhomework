package ru.inno.homeworklesson06;

import java.io.*;
import java.lang.reflect.Field;
import java.util.*;

/**
 * Сериализатор. Имплементирует SerializationUtility.
 */
public class Serialization implements SerializationUtility {
    @Override
    public void serialize(Object object, String file) {
        Class reflectionObject = object.getClass();
        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            String result = "<Object class=\"" + reflectionObject.getName() + "\">\r\n"
                    + fieldSerialization(reflectionObject.getDeclaredFields(), object) + "</Object>";
            byte[] buffer = result.getBytes();
            fileOutputStream.write(buffer);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object deSerialize(String file) {
        String result = "";
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            int i = fileInputStream.read();
            while (i != -1) {
                result += (char) i;
                i = fileInputStream.read();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<String> tags = new ArrayList<>();
        String[] elems = result.replaceAll("[\t]", "").split("[\r\n]");
        for (String elem : elems) {
            if (!elem.equals("")) {
                tags.add(elem);
            }
        }
        Object obj = null;
        String className = tags.get(0).substring(tags.get(0).indexOf("\"") + 1, tags.get(0).indexOf(">") - 1);
        try {
            Class reflectionObject = Class.forName(className);
            obj = reflectionObject.newInstance();
            for (int i = 1; i < tags.size() - 1; i++) {
                String fieldName = tags.get(i).substring(
                        tags.get(i).indexOf("name") + 6, tags.get(i).indexOf("type") - 2);
                Field field = reflectionObject.getDeclaredField(fieldName);
                field.setAccessible(true);
                String value = tags.get(i).substring(tags.get(i).indexOf(">") + 1, tags.get(i).indexOf("</"));
                String type = tags.get(i).substring(tags.get(i).indexOf("type") + 6, tags.get(i).indexOf(">") - 1);
                switch (type) {
                    case "short": {
                        field.setShort(obj, Short.valueOf(value));
                        break;
                    }
                    case "int": {
                        field.setInt(obj, Integer.valueOf(value));
                        break;
                    }
                    case "long": {
                        field.setLong(obj, Long.valueOf(value));
                        break;
                    }
                    case "float": {
                        field.setFloat(obj, Float.valueOf(value));
                        break;
                    }
                    case "double": {
                        field.setDouble(obj, Double.valueOf(value));
                        break;
                    }
                    case "byte": {
                        field.setByte(obj, Byte.valueOf(value));
                        break;
                    }
                    case "boolean": {
                        field.setBoolean(obj, Boolean.valueOf(value));
                        break;
                    }
                    case "char": {
                        field.setChar(obj, value.charAt(0));
                        break;
                    }
                    case "java.lang.String": {
                        field.set(obj, value);
                        break;
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return obj;
    }

    /**
     * Сериализация полей объекта
     *
     * @param fields сериализуемые поля
     * @param object сериализуемый объект
     * @return поля объекта, представленные в виде строки
     */
    private String fieldSerialization(Field[] fields, Object object) throws IllegalAccessException {
        String resultOfFieldSerialization = "";
        for (Field field : fields) {
            field.setAccessible(true);
            resultOfFieldSerialization += "\t<field name=\"" + field.getName()
                    + "\" type=\"" + field.getType().getName() + "\">" + field.get(object) + "</field>\r\n";

        }
        return resultOfFieldSerialization;
    }
}
