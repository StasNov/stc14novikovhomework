package ru.inno.homeworklesson06;

/**
 * Сериализуемый класс
 * Содержит информацию о каком-либо фильме
 */
public class Film {
    private String name;
    private int year;
    private String country;
    private String filmDirector;
    private int budget;
    private int boxOffice;

    public Film() {
    }

    /**
     * Конструктор
     *
     * @param name         название
     * @param year         год выпуска на экраны кинотеатров
     * @param country      страна производства/съёмок
     * @param filmDirector режиссёр
     * @param budget       бюджет
     * @param boxOffice    кассовые сборы
     */
    public Film(String name, int year, String country, String filmDirector, int budget, int boxOffice) {
        this.name = name;
        this.year = year;
        this.country = country;
        this.filmDirector = filmDirector;
        this.budget = budget;
        this.boxOffice = boxOffice;
    }

    @Override
    public String toString() {
        return "Film{" +
                "name='" + name + '\'' +
                ", year=" + year +
                ", country='" + country + '\'' +
                ", filmDirector='" + filmDirector + '\'' +
                ", budget=" + budget +
                ", boxOffice=" + boxOffice +
                '}';
    }
}
