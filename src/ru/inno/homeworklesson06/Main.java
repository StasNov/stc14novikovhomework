package ru.inno.homeworklesson06;


public class Main {
    public static void main(String[] args) {
        Film film = new Film("Bohemian Rhapsody", 2018, "Britain", "Bryan Singer",
                52_000_000, 798_453_776);
        System.out.println(film);
        System.out.println("Start of serialization");
        Serialization serialization = new Serialization();
        serialization.serialize(film, "src/ru/inno/homeworklesson06/film.xml");
        System.out.println("Finish of serialization");
        System.out.println("Start of deserialization");
        Film anotherFilm = (Film) serialization.deSerialize("src/ru/inno/homeworklesson06/film.xml");
        System.out.println("Finish of deserialization");
        System.out.println(anotherFilm);
    }
}