package ru.inno.homeworklesson06;

/**
 * Сериализационная утилита
 */
public interface SerializationUtility {

    /**
     * Сериализация объекта
     *
     * @param object сериализуемый объект;
     * @param file   файл, в который будет записан сериализуемый объект
     */
    void serialize(Object object, String file);

    /**
     * Десериализация объекта
     *
     * @param file файл, в котором записан сериализованный объект подлежащий десериализации
     * @return десериализованный объект
     */
    Object deSerialize(String file);
}
