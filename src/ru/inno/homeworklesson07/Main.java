package ru.inno.homeworklesson07;

import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException,
            InstantiationException, NoSuchMethodException, InvocationTargetException {
        WriterInFile writer = new WriterInFile();
        writer.writeInFile("src/ru/inno/homeworklesson07/someclasses/SomeClass.java");
        JavaFileCompiler javac = new JavaFileCompiler();
        javac.compile("src/ru/inno/homeworklesson07/someclasses/SomeClass.java");
        ClassLoader classLoader = new MyClassLoader();
        Class<?> someClass = classLoader.loadClass("ru.inno.homeworklesson07.someclasses.SomeClass");
        Worker doer = (Worker) someClass.newInstance();
        someClass.getMethod("doWork").invoke(doer, null);
    }
}
