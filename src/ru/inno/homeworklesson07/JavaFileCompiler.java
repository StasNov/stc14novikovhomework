package ru.inno.homeworklesson07;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

/**
 * Компилятор java-файлов
 */
public class JavaFileCompiler implements Compiler {
    @Override
    public void compile(String fileName) {
        JavaCompiler javac = ToolProvider.getSystemJavaCompiler();
        String[] javacOpts = {fileName};
        javac.run(null, null, null, javacOpts);
    }
}
