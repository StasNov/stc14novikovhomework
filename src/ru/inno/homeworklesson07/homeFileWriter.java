package ru.inno.homeworklesson07;

/**
 * Записыватель файлов
 */
public interface homeFileWriter {

    /**
     * Запись в файл
     *
     * @param path путь к файлу в который будет осуществляться запись
     */
    void writeInFile(String path);
}
