package ru.inno.homeworklesson07;

/**
 * Компилятор
 */
public interface Compiler {

    /**
     * Компиляция
     *
     * @param fileName название компилируемого файла
     */
    void compile(String fileName);
}
