package ru.inno.homeworklesson07;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Записыватель в файл
 */
public class WriterInFile implements homeFileWriter {

    @Override
    public void writeInFile(String path) {
        Scanner scanner = new Scanner(System.in);
        String input = "";
        while (true) {
            String str = scanner.nextLine();
            if (str.equals("")){
                break;
            }
            input += str;
        }
        String result = "package ru.inno.homeworklesson07.someclasses;\r\n";
        result += "import ru.inno.homeworklesson07.Worker;\r\n";
        result += "public class SomeClass implements Worker {\r\n";
        result += "\t@Override\r\n";
        result += "\tpublic void doWork() {\r\n";
        result += "\t\t" + input + "\r\n";
        result += "\t}\r\n}";
        try {
            Files.write(Paths.get(path), result.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
