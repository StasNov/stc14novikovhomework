package ru.inno.homeworklesson07;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Загрузчик класса
 */
public class MyClassLoader extends ClassLoader {

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        String className = "ru.inno.homeworklesson07.someclasses.SomeClass";
        if (className.equals(name)) {
            try {
                byte[] bytes = Files.readAllBytes(
                        Paths.get("./src/ru/inno/homeworklesson07/someclasses/SomeClass.class"));
                return defineClass(name, bytes, 0, bytes.length);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return super.findClass(name);
    }
}
