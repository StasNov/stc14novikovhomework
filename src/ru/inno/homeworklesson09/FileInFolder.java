package ru.inno.homeworklesson09;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Поисковик файлов в папке
 */
public class FileInFolder {

    /**
     * Получение списка путей к файлам, хранящихся в папке
     *
     * @param folderName путь к папке
     * @return массив строк, хранящий список путей к файлам в указанной папке
     */
    public String[] getFilesFromFolder(String folderName) {
        File folder = new File(folderName);
        File[] listOfFiles = folder.listFiles();
        List<String> fileNames = new ArrayList<>();
        Stream.of(listOfFiles).forEach(value -> fileNames.add(String.valueOf(value)));
        return fileNames.toArray(new String[fileNames.size()]);
    }
}
