package ru.inno.homeworklesson09;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Поисковик слов.
 * В качестве парсера использует класс FileParserCall, который имплементирует Callable
 */
public class WordFinderCall implements Finder {
    private StringBuilder resultString;
    private int pools;

    /**
     * Конструктор
     *
     * @param pools количество потоков
     */
    public WordFinderCall(int pools) {
        this.resultString = new StringBuilder();
        this.pools = pools;
    }

    @Override
    public void getOccurencies(String[] sources, String[] words, String res) {
        ThreadPool threadPool = new ThreadPool();
        try {
            threadPool.createPools(sources, words, resultString, pools);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        saveInFile(res);
    }

    /**
     * Запись в файл
     *
     * @param res путь к файлу
     */
    private void saveInFile(String res) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(res))) {
            writer.write(resultString.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
