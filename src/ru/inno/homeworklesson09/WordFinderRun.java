package ru.inno.homeworklesson09;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Поисковик слов.
 * В качестве парсера использует класс FileParserRun, который имплементирует Runnable
 */
public class WordFinderRun implements Finder {
    private List<String> result;
    private List<Thread> threads;

    public WordFinderRun() {
        this.result = new ArrayList<>();
        this.threads = new ArrayList<>();
    }

    @Override
    public void getOccurencies(String[] sources, String[] words, String res) {
        Stream.of(sources).forEach(value -> threads.add(new Thread(new FileParserRun(result, value, words))));
        for (Thread thread : threads) {
            thread.start();
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        saveInFile(res);
    }

    /**
     * Запись в файл
     *
     * @param res путь к файлу
     */
    private void saveInFile(String res) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(res))) {
            for (String sentence : result) {
                writer.write(sentence);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
