package ru.inno.homeworklesson09;

import java.util.concurrent.Callable;

/**
 * Парсер файлов. Наследник FileParser. Имплементирует Callable.
 */
public class FileParserCall extends FileParser implements Callable<String> {

    public FileParserCall(String source, String[] words, StringBuilder resultString) {
        super(source, words, resultString);
    }

    @Override
    public String call() {
        find(true);
        return resultString.toString();
    }

}
