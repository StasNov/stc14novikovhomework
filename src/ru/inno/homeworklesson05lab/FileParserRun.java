package ru.inno.homeworklesson05lab;

import java.util.List;

/**
 * Парсер файлов. Наследник FileParser. Имплементирует Runnable.
 * Обрабатывает каждый файл в отдельном потоке.
 */
public class FileParserRun extends FileParser implements Runnable{


    public FileParserRun(List<String> result, String source, String[] words) {
        super(result, source, words);
    }

    @Override
    public void run() {
        find(false);
    }

}
