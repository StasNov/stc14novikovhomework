package ru.inno.homeworklesson05lab;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Пул потоков
 */
public class ThreadPool {

    /**
     * Создание пула потоков
     *
     * @param sources      - массив текстовых файлов
     * @param words        - массив слов
     * @param resultString - объект StringBuilder для сохранения получившегося результата
     * @param pools        - количество потоков
     */
    public void createPools(String[] sources, String[] words, StringBuilder resultString, int pools) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(pools);
        List<Callable<String>> tasks = new ArrayList<>();
        for (String source : sources) {
            tasks.add(new FileParserCall(source, words, resultString));
        }
        executorService.invokeAll(tasks);
        executorService.shutdown();
    }
}
