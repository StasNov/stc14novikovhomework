package ru.inno.homeworklesson05lab;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Парсер файлов
 */
public class FileParser {
    protected List<String> result;
    protected String source;
    protected String[] words;
    protected StringBuilder resultString;

    public FileParser(List<String> result, String source, String[] words) {
        this.result = result;
        this.source = source;
        this.words = words;
    }

    public FileParser(String source, String[] words, StringBuilder resultString) {
        this.source = source;
        this.words = words;
        this.resultString = resultString;
    }

    /**
     * Поиск слов в предложениях файла
     *
     * @param callable - флаг, осуществляется ли вызов метода call класса
     *                 наследника FileParserCall
     */
    protected void find(boolean callable) {
        try (Scanner scanner = new Scanner(new File(source))) {
            Pattern regex = Pattern.compile("[\\?\\.\\!\\...]");
            scanner.useDelimiter(regex);
            while (scanner.hasNext()) {
                findWordsInSentence(scanner.next(), words, callable);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Поиск слов в предложении
     *
     * @param sentence - предложение, в котором будет осуществлять поиск слов из массива words
     * @param words    - массив слов
     * @param callable - флаг о том, осуществляется ли вызов метода call класса
     *                 наследника FileParserCall
     */
    private void findWordsInSentence(String sentence, String[] words, boolean callable) {
        sentence = sentence.trim().replaceAll("\\n", " ");
        String sentenceToUpperCase = sentence.toUpperCase();
        for (String word : words) {
            word = word.toUpperCase();
            if (sentenceToUpperCase.contains(word)) {
                if (callable) {
                    saveCallableSentence(sentence);
                } else {
                    saveSentence(sentence);
                }
                break;
            }
        }
    }

    /**
     * Строгий поиск слов в предложении
     *
     * @param sentence - предложение, в котором будет осуществлять поиск слов из массива words
     * @param words    - массив слов
     * @param callable - флаг о том, осуществляется ли вызов метода call класса
     *                 наследника FileParserCall
     */
    private void findWordsInSentenceStrong(String sentence, String[] words, boolean callable) {
        sentence = sentence.trim().replaceAll("\\n", " ").
                replaceAll("[\\s]{2,}", " ").replaceAll("[,]", "");
        String[] wordsFromSentence = sentence.split(" ");
        boolean checkSentence = false;
        for (String wordFromSenence : wordsFromSentence) {
            for (String word : words) {
                if (wordFromSenence.equalsIgnoreCase(word)) {
                    checkSentence = true;
                    break;
                }
            }
            if (checkSentence) {
                if (callable) {
                    saveCallableSentence(sentence);
                } else {
                    saveSentence(sentence);
                }
                break;
            }
        }
    }

    /**
     * Сохранение предложения.
     * Добавление в List<String> result
     *
     * @param sentence - сохраняемое предложение
     */
    private void saveSentence(String sentence) {
        synchronized (result) {
            result.add(sentence + ".\n");
        }
    }

    /**
     * Сохранение предложения.
     * Добавление в StringBuilder resultString
     *
     * @param sentence - сохраняемое предложение
     */
    private void saveCallableSentence(String sentence) {
        synchronized (resultString) {
            resultString.append(sentence + ".\n");
        }
    }
}
