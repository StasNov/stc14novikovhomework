package ru.inno.homeworklesson05lab;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Поисковик слов.
 * В качестве парсера использует класс FileParserCall, который имплементирует Callable
 */
public class WordFinderCall {
    private StringBuilder resultString;

    public WordFinderCall() {
        this.resultString = new StringBuilder();
    }

    /**
     * Поиск вхождения слов (из массива слов) в текстовые предложения, содержащиеся в файлах.
     * Запись предложений (в которых найдены слова) в файл, путь к которому указан в res.
     *
     * @param sources - массив текстовых файлов
     * @param words   - массив слов
     * @param res     - путь к файлу, в который будут записываться предложения
     * @param pools   - количество потоков
     */
    public void getOccurencies(String[] sources, String[] words, String res, int pools) throws InterruptedException {
        ThreadPool threadPool = new ThreadPool();
        threadPool.createPools(sources, words, resultString, pools);
        saveInFile(res);
    }

    /**
     * Запись в файл
     *
     * @param res - путь к файлу
     */
    private void saveInFile(String res) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(res))) {
            writer.write(resultString.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
