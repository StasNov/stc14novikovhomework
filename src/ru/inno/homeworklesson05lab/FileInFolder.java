package ru.inno.homeworklesson05lab;

import java.io.File;

/**
 * Поисковик файлов в папке
 */
public class FileInFolder {

    /**
     * Получение списка путей к файлам, хранящихся в папке
     *
     * @param folderName - путь к папке
     * @return массив строк, хранящий список путей к файлам в указанной папке
     */
    public String[] getFilesFromFolder(String folderName) {
        File folder = new File(folderName);
        File[] listOfFiles = folder.listFiles();
        String[] fileNames = new String[listOfFiles.length];
        for (int i = 0; i < listOfFiles.length; i++) {
            fileNames[i] = listOfFiles[i].getPath();
        }
        return fileNames;
    }
}
