package ru.inno.homeworklesson05lab;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Поисковик слов.
 * В качестве парсера использует класс FileParserRun, который имплементирует Runnable
 */
public class WordFinderRun {
    private List<String> result;
    private List<Thread> threads;

    public WordFinderRun() {
        this.result = new ArrayList<>();
        this.threads = new ArrayList<>();
    }

    /**
     * Поиск вхождения слов (из массива слов) в текстовые предложения, содержащиеся в файлах.
     * Запись предложений (в которых найдены слова) в файл, путь к которому указан в res.
     *
     * @param sources - массив текстовых файлов
     * @param words   - массив слов
     * @param res     - путь к файлу, в который будут записываться предложения
     */
    public void getOccurencies(String[] sources, String[] words, String res) {
        for (String fileName : sources) {
            Thread parseSentencesThread = new Thread(new FileParserRun(result, fileName, words));
            parseSentencesThread.start();
            threads.add(parseSentencesThread);
        }
        for (Thread parseSentencesThread : threads) {
            try {
                parseSentencesThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        saveInFile(res);
    }

    /**
     * Запись в файл
     *
     * @param res - путь к файлу
     */
    private void saveInFile(String res) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(res))) {
            for (String sentence : result) {
                writer.write(sentence);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
