package ru.inno.homeworklesson05lab;


public class Main {
    /**
     * Исходные и результирующие файлы для bigFileRun(), manyFilesRun(), bigFileCall() не будут закоммичены и запушены
     * по причине большого размера или количества.
     */
    public static void main(String[] args) throws InterruptedException {
        warAndPeaceRun();
        bigFileRun();
        manyFilesRun();
        warAndPeaceCall();
        bigFileCall();
        manyFilesCall();
    }

    /**
     * Запуск WordFinderRun на файле war+peace.txt
     */
    private static void warAndPeaceRun() {
        String[] files = new FileInFolder().
                getFilesFromFolder("src/ru/inno/homeworklesson05lab/resourses/warandpeace/");
        String res = "src/ru/inno/homeworklesson05lab/result/ResultWarEndPeaceRun.txt";
        String[] words = {"Petersburg"};
        WordFinderRun wordFinderRun = new WordFinderRun();
        long beginTime = System.currentTimeMillis();
        wordFinderRun.getOccurencies(files, words, res);
        long endTime = System.currentTimeMillis();
        double resultTime = (endTime - beginTime) / 1000.0;
        System.out.println("\"War and Peace\". (Runnable) - Time of work - " + resultTime + "sec.");
    }

    /**
     * Запуск WordFinderRun на файле размером 1Гб
     */
    private static void bigFileRun() {
        String[] files = new FileInFolder().
                getFilesFromFolder("src/ru/inno/homeworklesson05lab/resourses/bigfile/");
        String res = "src/ru/inno/homeworklesson05lab/result/ResultAfterBigFileRun.txt";
        String[] words = {"Есть", "шесть", "жизнь"};
        WordFinderRun wordFinderRun = new WordFinderRun();
        long beginTime = System.currentTimeMillis();
        wordFinderRun.getOccurencies(files, words, res);
        long endTime = System.currentTimeMillis();
        double resultTime = (endTime - beginTime) / 1000.0;
        System.out.println("BigFile - (Runnable) Time of work - " + resultTime + "sec.");
    }

    /**
     * Запуск WordFinderRun на 2000 файлах.
     */
    private static void manyFilesRun() {
        String[] files = new FileInFolder().
                getFilesFromFolder("src/ru/inno/homeworklesson05lab/resourses/manyfiles/");
        String res = "src/ru/inno/homeworklesson05lab/result/ResultAfterManyFilesRun.txt";
        String[] words = {"человек", "плата", "свет", "голоса", "больная", "скоро", "вскочил", "опять", "вслед", "почти"};
        WordFinderRun wordFinderRun = new WordFinderRun();
        long beginTime = System.currentTimeMillis();
        wordFinderRun.getOccurencies(files, words, res);
        long endTime = System.currentTimeMillis();
        double resultTime = (endTime - beginTime) / 1000.0;
        System.out.println("ManyFiles (Runnable) - Time of work - " + resultTime + "sec.");
    }

    /**
     * Запуск WordFinderCall на файле war+peace.txt.
     */
    private static void warAndPeaceCall() throws InterruptedException {
        String[] files = new FileInFolder().
                getFilesFromFolder("src/ru/inno/homeworklesson05lab/resourses/warandpeace/");
        String res = "src/ru/inno/homeworklesson05lab/result/ResultWarEndPeaceCallable.txt";
        String[] words = {"Petersburg"};
        WordFinderCall wordFinderCall = new WordFinderCall();
        long beginTime = System.currentTimeMillis();
        wordFinderCall.getOccurencies(files, words, res, 10);
        long endTime = System.currentTimeMillis();
        double resultTime = (endTime - beginTime) / 1000.0;
        System.out.println("\"War and Peace\". (Callable) - Time of work - " + resultTime + "sec.");
    }

    /**
     * Запуск WordFinderCall на файле размером 1Гб.
     */
    private static void bigFileCall() throws InterruptedException {
        String[] files = new FileInFolder().
                getFilesFromFolder("src/ru/inno/homeworklesson05lab/resourses/bigfile/");
        String res = "src/ru/inno/homeworklesson05lab/result/ResultAfterBigFileCallable.txt";
        String[] words = {"Есть", "шесть", "жизнь"};
        WordFinderCall wordFinderCall = new WordFinderCall();
        long beginTime = System.currentTimeMillis();
        wordFinderCall.getOccurencies(files, words, res, 10);
        long endTime = System.currentTimeMillis();
        double resultTime = (endTime - beginTime) / 1000.0;
        System.out.println("BigFile - (Callable) Time of work - " + resultTime + "sec.");
    }

    /**
     * Запуск WordFinderCall на нескольких файлах.
     */
    private static void manyFilesCall() throws InterruptedException {
        String[] files = new FileInFolder().
                getFilesFromFolder("src/ru/inno/homeworklesson05lab/resourses/middlecountoffiles/");
        String res = "src/ru/inno/homeworklesson05lab/result/ResultAfterMiddleCountOfFiles.txt";
        String[] words = {"человек", "плата", "свет", "голоса", "больная", "скоро", "вскочил", "опять", "вслед", "почти"};
        WordFinderCall wordFinderCall = new WordFinderCall();
        long beginTime = System.currentTimeMillis();
        wordFinderCall.getOccurencies(files, words, res, 2);
        long endTime = System.currentTimeMillis();
        double resultTime = (endTime - beginTime) / 1000.0;
        System.out.println("ManyFiles (Callable) - Time of work - " + resultTime + "sec.");
    }
}
