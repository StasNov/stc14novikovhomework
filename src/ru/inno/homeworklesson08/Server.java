package ru.inno.homeworklesson08;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Сервер.
 */
public class Server {
    public static final int PORT = 3232;
    public static List<ServerClientCommunication> clients = new ArrayList<>();

    /**
     * Запуск сервера
     */
    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            System.out.println("Сервер начал работу");
            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("Новый пользователь присоединился к чату: " + socket);
                ServerClientCommunication serverClient = new ServerClientCommunication(socket);
                clients.add(serverClient);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
