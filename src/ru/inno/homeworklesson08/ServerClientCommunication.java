package ru.inno.homeworklesson08;

import java.io.*;
import java.net.Socket;

/**
 * Поток взаимодействия сервера и клиента
 */
public class ServerClientCommunication extends Thread {
    private Socket socket;
    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;

    /**
     * Конструктор
     *
     * @param socket сокет через который осуществляется взаимодействие сервера и клиента
     */
    public ServerClientCommunication(Socket socket) throws IOException {
        this.socket = socket;
        bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        start();
    }

    @Override
    public void run() {
        String message;
        try {
            while (true) {
                message = bufferedReader.readLine();
                if (message.equals("quit")) {
                    throw new Exception();
                }
                System.out.println("Сообщение от " + message);
                for (ServerClientCommunication serverClient : Server.clients) {
                    if (!serverClient.equals(this)) {
                        serverClient.send(message);
                    }
                }
            }
        } catch (Exception e) {
            Server.clients.remove(this);
            System.out.println("Участник " + socket + " покинул чат");
            try {
                bufferedReader.close();
                bufferedWriter.close();
                socket.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }


    /**
     * Отправка сообщения
     *
     * @param msg отправляемое сообщение
     */
    private void send(String msg) {
        try {
            bufferedWriter.write(msg + "\n");
            bufferedWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
