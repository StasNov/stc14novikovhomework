package ru.inno.homeworklesson08;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Слушатель сообщений чата
 */
public class Listener extends Thread {
    private Socket socket;

    /**
     * Конструктор
     *
     * @param socket прослушиваемый сокет
     */
    public Listener(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
            String msg;
            while (true) {
                msg = bufferedReader.readLine();
                System.out.println(msg);
            }
        } catch (IOException e) {
            System.out.println("Вы вышли из чата!");
        }
    }
}
