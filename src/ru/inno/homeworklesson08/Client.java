package ru.inno.homeworklesson08;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * Клиент
 */
public class Client {

    /**
     * Запуск клиента
     */
    public static void main(String[] args) {
        try (Socket socket = new Socket("127.0.0.1", Server.PORT)) {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            Listener listener = new Listener(socket);
            listener.start();
            Scanner scanner = new Scanner(System.in);
            System.out.println("Введите Ваше имя: ");
            String name = scanner.nextLine();
            System.out.println("Привет, " + name);
            String message;
            while (true) {
                message = scanner.nextLine();
                if (message.equals("quit")) {
                    break;
                }
                writer.write(name + ": " + message + "\n");
                writer.flush();
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
