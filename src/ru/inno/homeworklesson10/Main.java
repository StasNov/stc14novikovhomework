package ru.inno.homeworklesson10;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Заполнение heap space объектами до тех пор, пока heap space не заполнится целиком.
 * В случае заполнения heap space программа завершается с ошибкой OutOfMemoryError c пометкой Java Heap Space.
 */
public class Main {
    public static void main(String[] args) {
        List<Object> listOfObjects = new ArrayList<>();
        Random rnd = new Random();
        while (true) {
            listOfObjects.add(new Object());
            if (rnd.nextInt(100) % 5 == 0) {
                listOfObjects.remove(listOfObjects.size() - 1);
            }
        }
    }
}
