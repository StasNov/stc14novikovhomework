package ru.inno.homeworklesson14;


public class Main {
    /**
     * Содержимое папок bigfile и manyfiles, как и результаты обработки содержимого этих папок, не будет закоммичено и
     * запушено по причине большого размера или количества.
     */
    public static void main(String[] args) throws InterruptedException {

        String[] wordsForWarAndPeace = {"Petersburg"};
        String[] wordsForManyFiles = {"человек", "плата", "свет", "голоса", "больная", "скоро", "вскочил", "опять",
                "вслед", "почти"};


        //Запуск WordFinderRun на файле war+peace.txt
        searcher("src/ru/inno/homeworklesson14/resources/warandpeace/", wordsForWarAndPeace,
                "src/ru/inno/homeworklesson14/result/ResultWarEndPeaceRun.txt",
                "\"War and Peace\". (Runnable) - Time of work - ");

        //Запуск WordFinderCall на файле war+peace.txt.
        searcher("src/ru/inno/homeworklesson14/resources/warandpeace/", wordsForWarAndPeace,
                "src/ru/inno/homeworklesson14/result/ResultWarEndPeaceCallable.txt",
                "\"War and Peace\". (Callable) - Time of work - ", 20);

        //Запуск WordFinderCall на нескольких файлах.
        searcher("src/ru/inno/homeworklesson14/resources/middlecountoffiles/", wordsForManyFiles,
                "src/ru/inno/homeworklesson14/result/ResultAfterMiddleCountOfFiles.txt",
                "ManyFiles (Callable) - Time of work - ", 2);
    }

    /**
     * Создание экземпляра класса WordFinderRun
     *
     * @param folderName папка с файлами, содержащими предложения в которых осуществляется поиск слов массива words
     * @param words      массив слов
     * @param res        путь к файлу куда записываются предложения в которых было найдено хотя бы одно слово из
     *                   массива words
     * @param resMessage сообщение о завершении работы метода
     */
    private static void searcher(String folderName, String[] words, String res, String resMessage) {
        Finder wordFinder = new WordFinderRun();
        mainActionsForSearch(wordFinder, folderName, words, res, resMessage);
    }


    /**
     * Создание экземпляра класса WordFinderCall
     *
     * @param folderName папка с файлами, содержащими предложения в которых осуществляется поиск слов массива words
     * @param words      массив слов
     * @param res        путь к файлу куда записываются предложения в которых было найдено хотя бы одно слово из
     *                   массива words
     * @param resMessage сообщение о завершении работы метода
     * @param pools      количество потоков. Если @param pools <= 0, то выбрасывается исключение
     *                   IllegalArgumentException
     */
    private static void searcher(String folderName, String[] words, String res, String resMessage, int pools) {
        Finder wordFinder = new WordFinderCall(pools);
        mainActionsForSearch(wordFinder, folderName, words, res, resMessage);
    }

    /**
     * Поиск вхождения слов (из массива words) в текстовые предложения, содержащиеся в файлах,
     * которые хранятся в папке folderName. Предложения, в которых найдены слова, записываются в файл,
     * путь к которому указан в res.
     *
     * @param wordFinder экземпляр класса имплементирующего интерфейс Finder
     * @param folderName папка с файлами, содержащими предложения в которых осуществляется поиск слов массива words
     * @param words      массив слов
     * @param res        путь к файлу куда записываются предложения в которых было найдено хотя бы одно слово из
     *                   массива words
     * @param resMessage сообщение о завершении работы метода
     */
    private static void mainActionsForSearch(Finder wordFinder, String folderName, String[] words, String res,
                                             String resMessage) {
        String[] files = new FileInFolder().getFilesFromFolder(folderName);
        long beginTime = System.currentTimeMillis();
        wordFinder.getOccurencies(files, words, res);
        long endTime = System.currentTimeMillis();
        double resultTime = (endTime - beginTime) / 1000.0;
        System.out.println(resMessage + resultTime + "sec.");
    }
}
