package ru.inno.homeworklesson14;

public interface Finder {

    /**
     * Поиск вхождения слов (из массива слов) в текстовые предложения, содержащиеся в файлах.
     * Запись предложений (в которых найдены слова) в файл, путь к которому указан в res.
     *
     * @param sources массив текстовых файлов
     * @param words   массив слов
     * @param res     путь к файлу, в который будут записываться предложения
     */
    void getOccurencies(String[] sources, String[] words, String res);
}
