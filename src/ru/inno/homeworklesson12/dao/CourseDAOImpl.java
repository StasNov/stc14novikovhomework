package ru.inno.homeworklesson12.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.inno.homeworklesson12.ConnectionManager.ConnectionManager;
import ru.inno.homeworklesson12.ConnectionManager.ConnectionManagerImpl;
import ru.inno.homeworklesson12.entity.Person;
import ru.inno.homeworklesson12.entity.Subject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Realization CourseDAO
 */
public class CourseDAOImpl implements CourseDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(CourseDAOImpl.class);

    private static final String SELECT_PERSON_ID_FROM_COURSE = "SELECT person_id FROM course WHERE subject_id = ?";
    private static final String SELECT_FROM_PERSON = "SELECT * FROM person WHERE person_id = ?";
    private static final String SELECT_SUBJECT_ID_FROM_COURSE = "SELECT subject_id FROM course WHERE person_id = ?";
    private static final String SELECT_FROM_SUBJECT = "SELECT * FROM subject WHERE subject_id = ?";
    private static final String INSERT_INTO_COURSE = "INSERT INTO course (person_id, subject_id) VALUES (?, ?)";
    private ConnectionManager connectionManager = new ConnectionManagerImpl();


    @Override
    public Collection<Person> getPersonsBySubject(Subject subject) {
        Collection<Person> persons = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection()) {
            LOGGER.info("Начало формирования списка слушателей курса по указанному предмету - {}",
                    subject.getDescription());
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_PERSON_ID_FROM_COURSE);
            preparedStatement.setInt(1, subject.getId());
            ResultSet resultFromCourse = preparedStatement.executeQuery();
            while (resultFromCourse.next()) {
                PreparedStatement preparedStatementPerson = connection.prepareStatement(SELECT_FROM_PERSON);
                preparedStatementPerson.setInt(1, resultFromCourse.getInt(1));
                ResultSet resultFromPerson = preparedStatementPerson.executeQuery();
                if (resultFromPerson.next()) {
                    Person person = new Person(resultFromPerson.getInt(1),
                            resultFromPerson.getString(2), resultFromPerson.getDate(3).getTime());
                    ((ArrayList<Person>) persons).add(person);
                }
            }
            LOGGER.info("Завершение формирования списка слушателей курса по указанному предмету - {}",
                    subject.getDescription());
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.error("Не удалось сформировать список слушателей курса по указанному предмету - {}. Ошибка {}",
                    subject.getDescription(), e.getMessage());
        }
        return persons;
    }

    @Override
    public Collection<Subject> getSubjectsByPerson(Person person) {
        Collection<Subject> subjects = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection()) {
            LOGGER.info("Начало формирования списка предметов указанного пользователя - {}", person.getName());
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_SUBJECT_ID_FROM_COURSE);
            preparedStatement.setInt(1, person.getId());
            ResultSet resultFromCourse = preparedStatement.executeQuery();
            while (resultFromCourse.next()) {
                PreparedStatement preparedStatementSubject = connection.prepareStatement(SELECT_FROM_SUBJECT);
                preparedStatementSubject.setInt(1, resultFromCourse.getInt(1));
                ResultSet resultFromSubject = preparedStatementSubject.executeQuery();
                if (resultFromSubject.next()) {
                    Subject subject = new Subject(resultFromSubject.getInt(1),
                            resultFromSubject.getString(2));
                    ((ArrayList<Subject>) subjects).add(subject);
                }
            }
            LOGGER.info("Завершение формирования списка предметов указанного пользователя - {}", person.getName());
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.error("Не удалось сформировать список предметов указанного пользователя - {}. Ошибка - {}",
                    person.getName(), e.getMessage());
        }
        return subjects;
    }

    @Override
    public boolean linkPersonWithCourse(Person person, Subject subject) {
        boolean resultLink = false;
        try (Connection connection = connectionManager.getConnection()) {
            LOGGER.info("Начало осуществления записи указанного пользователя - {} на указанный предмет - {}",
                    person.getName(), subject.getDescription());
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_INTO_COURSE);
            preparedStatement.setInt(1, person.getId());
            preparedStatement.setInt(2, subject.getId());
            preparedStatement.execute();
            resultLink = true;
            LOGGER.info("Завершение записи указанного пользователя - {} на указанный предмет - {}", person.getName(),
                    subject.getDescription());
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.error("Не удалось записать указанного пользователя - {} на указанный предмет - {}. Ошибка - {}",
                    person.getName(), subject.getDescription(), e.getMessage());
        }
        return resultLink;
    }
}
