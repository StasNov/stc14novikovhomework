package ru.inno.homeworklesson12.dao;

import ru.inno.homeworklesson12.entity.Person;

import java.util.Collection;


/**
 * Data Access Object for Person
 */
public interface PersonDAO {

    /**
     * Добавление Person в базу данных
     *
     * @param person объект Person
     * @return результат выполнения добавления (true - добавление было выполнено / false - добавление выполнено не было)
     */
    boolean add(Person person);

    /**
     * Получение Person из базы данных по id
     *
     * @param id идентификатор Person
     * @return объект Person
     */
    Person getById(int id);

    /**
     * Обновление Person в базе данных
     *
     * @param person объект Person
     * @return результат выполнения обновления (true - обновление было выполнено / false - обновление выполнено не было)
     */
    boolean update(Person person);

    /**
     * Удаление Person из базы данных по id
     *
     * @param id идентификатор Person
     * @return результат выполнения удаления (true - удаление было выполнено / false - удаление выполнено не было)
     */
    boolean deleteById(int id);

    /**
     * Получение всех Person из базы данных
     *
     * @return коллекция всех объектов Person, информация о которых хранится в базе данных
     */
    Collection<Person> getAllPersons();
}
