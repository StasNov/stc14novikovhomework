package ru.inno.homeworklesson12.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.inno.homeworklesson12.ConnectionManager.ConnectionManager;
import ru.inno.homeworklesson12.ConnectionManager.ConnectionManagerImpl;
import ru.inno.homeworklesson12.entity.Subject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Realization SubjectDAO
 */
public class SubjectDAOImpl implements SubjectDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(SubjectDAOImpl.class);

    private static final String INTO_SUBJECT_VALUES = "INSERT INTO subject (description) VALUES (?)";
    private static final String SELECT_FROM_SUBJECT_WHERE_SUBJECT_ID = "SELECT * FROM subject WHERE subject_id = ?";
    private static final String UPDATE_SUBJECT = "WITH rows AS " +
            "(UPDATE subject SET description = ? WHERE subject_id = ? returning 1) SELECT count(*) FROM rows";
    private static final String DELETE_FROM_SUBJECT = "WITH rows AS " +
            "(DELETE FROM subject WHERE subject_id = ? returning 1) SELECT count(*) FROM rows";
    private static final String SELECT_FROM_SUBJECT = "SELECT * FROM subject";
    private ConnectionManager connectionManager = new ConnectionManagerImpl();


    @Override
    public boolean add(Subject subject) {
        boolean resultAdd = false;
        try (Connection connection = connectionManager.getConnection()) {
            LOGGER.info("Осуществление добавления записи о предмете - {} в базу данных", subject.getDescription());
            PreparedStatement preparedStatement = connection.prepareStatement(INTO_SUBJECT_VALUES);
            preparedStatement.setString(1, subject.getDescription());
            preparedStatement.execute();
            resultAdd = true;
            LOGGER.info("Завершение добавления записи о предмете - {} в базу данных", subject.getDescription());
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.error("Не удалось добавить запись о предмете - {} в базу данных. Ошибка - {}",
                    subject.getDescription(), e.getMessage());
        }
        return resultAdd;
    }

    @Override
    public Subject getById(int id) {
        Subject subject = null;
        try (Connection connection = connectionManager.getConnection()) {
            LOGGER.info("Начало формирования данных о предмете по указанному id - {}", id);
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_FROM_SUBJECT_WHERE_SUBJECT_ID);
            preparedStatement.setInt(1, id);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                subject = new Subject(result.getInt(1), result.getString(2));
            }
            LOGGER.info("Завершение формирования данных о предмете по указанному id - {}", id);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.error("Не удалось сформировать данные о предмете по указанному id - {}. " +
                    "Ошибка - {}", id, e.getMessage());
        }
        return subject;
    }

    @Override
    public boolean update(Subject subject) {
        boolean resultUpdate = false;
        try (Connection connection = connectionManager.getConnection()) {
            LOGGER.info("Начало осуществления обновления информации в базе данных о предмете - {}",
                    subject.getDescription());
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SUBJECT);
            preparedStatement.setString(1, subject.getDescription());
            preparedStatement.setInt(2, subject.getId());
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                if (result.getInt(1) > 0) {
                    resultUpdate = true;
                }
            }
            LOGGER.info("Завершение обновления информации в базе данных о предмете - {}", subject.getDescription());
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.error("Не удалось обновить информацию в базе данных о предмете - {}. Ошибка - {}",
                    subject.getDescription(), e.getMessage());
        }
        return resultUpdate;
    }

    @Override
    public boolean deleteById(int id) {
        boolean resultDelete = false;
        try (Connection connection = connectionManager.getConnection()) {
            LOGGER.info("Начало осуществления удаления информации о предмете из базы данных по указанному id - {}", id);
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_FROM_SUBJECT);
            preparedStatement.setInt(1, id);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                if (result.getInt(1) > 0) {
                    resultDelete = true;
                }
            }
            LOGGER.info("Завершение удаления информации о предмете из базы данных по указанному id - {}", id);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.error("Не удалось удалить информацию о предмете из базы данных по указанному id - {}. Ошибка - {}",
                    id, e.getMessage());
        }
        return resultDelete;
    }

    @Override
    public Collection<Subject> getAllSubjects() {
        Collection<Subject> subjects = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection()) {
            LOGGER.info("Начало формирования списка всех предметов, информация о которых хранится в базе данных");
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_FROM_SUBJECT);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                Subject subject = new Subject(result.getInt(1), result.getString(2));
                ((ArrayList<Subject>) subjects).add(subject);
            }
            LOGGER.info("Завершение формирования списка всех предметов, информация о которых хранится в базе данных");
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.error("Не удалось сформировать список всех предметов, информация о которых хранится в " +
                    "базе данных. Ошибка - {}", e.getMessage());
        }
        return subjects;
    }
}
