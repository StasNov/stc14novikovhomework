package ru.inno.homeworklesson12.dao;

import ru.inno.homeworklesson12.entity.Person;
import ru.inno.homeworklesson12.entity.Subject;

import java.util.Collection;

/**
 * Data Access Object for Course
 */
public interface CourseDAO {

    /**
     * Получение коллекции объектов Person, связанных с указанным объектом Subject
     *
     * @param subject объект Subject
     * @return коллекция объектов Person
     */
    Collection<Person> getPersonsBySubject(Subject subject);

    /**
     * Получение коллекции объектов Subject, связанных с указанным объектом Person
     *
     * @param person объект Person
     * @return коллекция объектов Subject
     */
    Collection<Subject> getSubjectsByPerson(Person person);

    /**
     * Установление связи указанного объекта Person с указанным объектом Subject
     *
     * @param person  объект Person
     * @param subject объект Subject
     * @return результат выполнения установления связи (true - связь была выполнена / false - связь не была установлена)
     */
    boolean linkPersonWithCourse(Person person, Subject subject);
}
