package ru.inno.homeworklesson12.dao;

import ru.inno.homeworklesson12.entity.Subject;

import java.util.Collection;

/**
 * Data Access Object for Subject
 */
public interface SubjectDAO {

    /**
     * Добавление Subject в базу данных
     *
     * @param subject объект Subject
     * @return результат выполнения добавления (true - добавление было выполнено / false - добавление выполнено не было)
     */
    boolean add(Subject subject);

    /**
     * Получение Subject из базы данных по id
     *
     * @param id идентификатор Subject
     * @return объект Subject
     */
    Subject getById(int id);

    /**
     * Обновление Subject в базе данных
     *
     * @param subject объект Subject
     * @return результат выполнения обновления (true - обновление было выполнено / false - обновление выполнено не было)
     */
    boolean update(Subject subject);

    /**
     * Удаление Subject из базы данных по id
     *
     * @param id идентификатор Subject
     * @return результат выполнения удаления (true - удаление было выполнено / false - удаление выполнено не было)
     */
    boolean deleteById(int id);

    /**
     * Получение всех Subject из базы данных
     *
     * @return коллекция всех объектов Subject, информация о которых хранится в базе данных
     */
    Collection<Subject> getAllSubjects();
}
