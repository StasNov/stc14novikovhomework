package ru.inno.homeworklesson12.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.inno.homeworklesson12.ConnectionManager.ConnectionManager;
import ru.inno.homeworklesson12.ConnectionManager.ConnectionManagerImpl;
import ru.inno.homeworklesson12.entity.Person;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Realization PersonDAO
 */
public class PersonDAOImpl implements PersonDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonDAOImpl.class);

    private static final String INTO_PERSON_VALUES = "INSERT INTO person (name, birth_date) VALUES (?, ?)";
    private static final String SELECT_FROM_PERSON_WHERE_PERSON_ID = "SELECT * FROM person WHERE person_id = ?";
    private static final String UPDATE_PERSON = "WITH rows AS " +
            "(UPDATE person SET name = ?, birth_date = ? WHERE person_id = ? returning 1) SELECT count(*) FROM rows";
    private static final String DELETE_FROM_PERSON = "WITH rows AS " +
            "(DELETE FROM person WHERE person_id = ? returning 1) SELECT count(*) FROM rows";
    private static final String SELECT_FROM_PERSON = "SELECT * FROM person";
    private ConnectionManager connectionManager = new ConnectionManagerImpl();


    @Override
    public boolean add(Person person) {
        boolean resultAdd = false;
        try (Connection connection = connectionManager.getConnection()) {
            LOGGER.info("Осуществление добавления записи о пользователе - {} в базу данных", person.getName());
            PreparedStatement preparedStatement = connection.prepareStatement(INTO_PERSON_VALUES);
            preparedStatement.setString(1, person.getName());
            preparedStatement.setDate(2, new Date(person.getBirthDay()));
            preparedStatement.execute();
            resultAdd = true;
            LOGGER.info("Завершение добавления записи о пользователе - {} в базу данных", person.getName());
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.error("Не удалось добавить запись о пользователе - {} в базу данных. Ошибка - {}",
                    person.getName(), e.getMessage());
        }
        return resultAdd;
    }

    @Override
    public Person getById(int id) {
        Person person = null;
        try (Connection connection = connectionManager.getConnection()) {
            LOGGER.info("Начало формирования данных о пользователе по указанному id - {}", id);
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_FROM_PERSON_WHERE_PERSON_ID);
            preparedStatement.setInt(1, id);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                person = new Person(result.getInt(1), result.getString(2),
                        result.getDate(3).getTime());
            }
            LOGGER.info("Завершение формирования данных о пользователе по указанному id - {}", id);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.error("Не удалось сформировать данные о пользователе по указанному id - {}. " +
                    "Ошибка - {}", id, e.getMessage());
        }
        return person;
    }

    @Override
    public boolean update(Person person) {
        boolean resultUpdate = false;
        try (Connection connection = connectionManager.getConnection()) {
            LOGGER.info("Начало осуществления обновления информации в базе данных о пользователе - {}",
                    person.getName());
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_PERSON);
            preparedStatement.setString(1, person.getName());
            preparedStatement.setDate(2, new Date(person.getBirthDay()));
            preparedStatement.setInt(3, person.getId());
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                if (result.getInt(1) > 0) {
                    resultUpdate = true;
                }
            }
            LOGGER.info("Завершение обновления информации в базе данных о пользователе - {}", person.getName());
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.error("Не удалось обновить информацию в базе данных о пользователе - {}. Ошибка - {}",
                    person.getName(), e.getMessage());
        }
        return resultUpdate;
    }

    @Override
    public boolean deleteById(int id) {
        boolean resultDelete = false;
        try (Connection connection = connectionManager.getConnection()) {
            LOGGER.info("Начало осуществления удаления информации о пользователе из базы данных по указанному id - {}",
                    id);
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_FROM_PERSON);
            preparedStatement.setInt(1, id);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                if (result.getInt(1) > 0) {
                    resultDelete = true;
                }
            }
            LOGGER.info("Завершение удаления информации о пользователе из базы данных по указанному id - {}", id);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.error("Не удалось удалить информацию о пользователе из базы данных по указанному id - {}. " +
                    "Ошибка - {}", id, e.getMessage());
        }
        return resultDelete;
    }

    @Override
    public Collection<Person> getAllPersons() {
        Collection<Person> persons = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection()) {
            LOGGER.info("Начало формирования списка всех пользователей, информация о которых хранится в базе данных");
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_FROM_PERSON);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                Person person = new Person(result.getInt(1), result.getString(2),
                        result.getDate(3).getTime());
                ((ArrayList<Person>) persons).add(person);
            }
            LOGGER.info("Завершение формирования списка всех пользователей, информация о которых хранится в базе данных");
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.error("Не удалось сформировать список всех пользователей, информация о которых хранится в базе " +
                    "данных. Ошибка - {}", e.getMessage());
        }
        return persons;
    }
}
