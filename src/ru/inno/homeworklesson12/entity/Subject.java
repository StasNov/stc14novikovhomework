package ru.inno.homeworklesson12.entity;

import java.util.Objects;

/**
 * POJO Subject
 */
public class Subject {
    private int id;
    private String description;

    public Subject() {
    }

    public Subject(int id, String description) {
        this.id = id;
        this.description = description;
    }

    /**
     * Получить id
     *
     * @return id для Subject
     */
    public int getId() {
        return id;
    }

    /**
     * Получить описание
     *
     * @return описание для Subject
     */
    public String getDescription() {
        return description;
    }

    /**
     * Задать описание
     *
     * @param description описание для Subject
     */
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subject subject = (Subject) o;
        return id == subject.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Subject{" +
                "id=" + id +
                ", description='" + description + '\'' +
                '}';
    }
}
