package ru.inno.homeworklesson12.entity;

import java.util.Objects;

/**
 * POJO Person
 */
public class Person {
    private int id;
    private String name;
    private long birthDay;

    public Person() {
    }

    public Person(int id, String name, long birthDay) {
        this.id = id;
        this.name = name;
        this.birthDay = birthDay;
    }

    /**
     * Получить id
     *
     * @return id для Person
     */
    public int getId() {
        return id;
    }

    /**
     * Получить имя
     *
     * @return имя для Person
     */
    public String getName() {
        return name;
    }

    /**
     * Задать имя
     *
     * @param name имя для Person
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Получить дату рождения
     *
     * @return дата рождения для Person типа long
     */
    public long getBirthDay() {
        return birthDay;
    }

    /**
     * Задать дату рождения
     *
     * @param birthDay дата рождения для Person типа long
     */
    public void setBirthDay(long birthDay) {
        this.birthDay = birthDay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id == person.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthDay=" + birthDay +
                '}';
    }
}
