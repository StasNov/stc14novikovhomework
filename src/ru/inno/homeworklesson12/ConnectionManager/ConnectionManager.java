package ru.inno.homeworklesson12.ConnectionManager;

import java.sql.Connection;

/**
 * Менеджер подключения
 */
public interface ConnectionManager {

    /**
     * Получение подключения
     *
     * @return подключение к URL-адресу
     */
    Connection getConnection();
}