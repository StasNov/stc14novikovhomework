package ru.inno.homeworklesson12.ConnectionManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Realization ConnectionManager
 */
public class ConnectionManagerImpl implements ConnectionManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionManagerImpl.class);

    private static final String URL = "jdbc:postgresql://localhost:5433/postgres";
    private static final String LOGIN = "postgres";
    private static final String PASSWORD = "root";

    @Override
    public Connection getConnection() {
        Connection connection = null;
        try {
            LOGGER.info("Попытка установления соединения с базой данных");
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(URL, LOGIN, PASSWORD);
            LOGGER.info("Соединение с базой данных установлено");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            LOGGER.error("Соединение с базой данных не установлено. Ошибка - {}", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.error("Соединение с базой данных не установлено. Ошибка - {}", e.getMessage());
        }
        return connection;
    }
}
