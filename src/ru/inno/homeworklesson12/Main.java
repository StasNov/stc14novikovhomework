package ru.inno.homeworklesson12;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.inno.homeworklesson12.dao.*;
import ru.inno.homeworklesson12.entity.Person;
import ru.inno.homeworklesson12.entity.Subject;

import java.util.Collection;

public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        LOGGER.info("Начало работы программы");
        PersonDAO personDAOImpl = new PersonDAOImpl();
        SubjectDAO subjectDAOImpl = new SubjectDAOImpl();
        CourseDAO courseDAOImpl = new CourseDAOImpl();
        demonstrateWorkWithPesonDAO(personDAOImpl);
        demonstrateWorkWithSubjectDAO(subjectDAOImpl);
        demonstrateWorkWithCourseDAO(courseDAOImpl, personDAOImpl, subjectDAOImpl);
        LOGGER.info("Завершение работы программы");
    }

    /**
     * Демонстрация работы с PersonDAO.
     * Создание нескольких Person с последующим сохранением в базу данных.
     * Получение и вывод на экран всех Person, хранящихся в БД.
     * Удаление Person с именем "James Moriarty".
     * У Person с именем "Charles Baskerville" замена имени на "Henry Baskerville".
     * Сохранение изменений.
     * Получение и вывод на экран всех Person, хранящихся в БД после изменений.
     *
     * @param personDAOImpl объект PersonDAO
     */
    private static void demonstrateWorkWithPesonDAO(PersonDAO personDAOImpl) {
        String[] names = {"Sherlock Holmes", "John Watson", "James Moriarty",
                "Charles Baskerville"};
        for (String name : names) {
            Person person = createPerson(name);
            personDAOImpl.add(person);
        }

        Collection<Person> personsFromDB = personDAOImpl.getAllPersons();
        System.out.println("All persons before changes: " + personsFromDB);
        for (Person person : personsFromDB) {
            if ("James Moriarty".equals(person.getName())) {
                personDAOImpl.deleteById(person.getId());
            }
            if ("Charles Baskerville".equals(person.getName())) {
                person.setName("Henry Baskerville");
                personDAOImpl.update(person);
            }
        }
        System.out.println("All persons after changes: " + personDAOImpl.getAllPersons());
    }

    /**
     * Демонстрация работы с SubjectDAO.
     * Создание нескольких Subject с последующим сохранением в базу данных.
     * Получение и вывод на экран всех Subject, хранящихся в БД.
     * Удаление Subject с описанием "C++".
     * У Subject с описанием "PHP" замена описания на "Java".
     * Сохранение изменений.
     * Получение и вывод на экран всех Subject, хранящихся в БД после изменений.
     *
     * @param subjectDAOImpl объект subjectDAO
     */
    private static void demonstrateWorkWithSubjectDAO(SubjectDAO subjectDAOImpl) {
        String[] subjectDescriptions = {"Python", "C++", "PHP"};
        for (String subjectDescription : subjectDescriptions) {
            Subject subject = createSubject(subjectDescription);
            subjectDAOImpl.add(subject);
        }

        Collection<Subject> subjectsFromDB = subjectDAOImpl.getAllSubjects();
        System.out.println("All subjects before changes: " + subjectsFromDB);
        for (Subject subject : subjectsFromDB) {
            if ("C++".equals(subject.getDescription())) {
                subjectDAOImpl.deleteById(subject.getId());
            }

            if ("PHP".equals(subject.getDescription())) {
                subject.setDescription("Java");
                subjectDAOImpl.update(subject);
            }
        }
        System.out.println("All subjects after changes: " + subjectDAOImpl.getAllSubjects());
    }

    /**
     * Демонстрация работы с CourseDAO.
     * Получение значения id для Person с именем "Sherlock Holmes".
     * Получение значения id для Person с именем "John Watson".
     * Получение значения id для Person с именем "Henry Baskerville".
     * Получение значения id для Subject с описанием "Java".
     * Получение значения id для Subject с описанием "Python".
     * Запись Person с именем "Sherlock Holmes" на Subject с описанием "Java".
     * Запись Person с именем "Sherlock Holmes" на Subject с описанием "Python".
     * Запись Person с именем "John Watson" на Subject с описанием "Java".
     * Запись Person с именем "Henry Baskerville" на Subject с описанием "Python".
     * Вывод на экран участников курса Subject с описанием "Java".
     * Вывод на экран участников курса Subject с описанием "Python".
     * Вывод на экран курсов, на которые записан(а) Person с именем "Sherlock Holmes".
     * Вывод на экран курсов, на которые записан(а) Person с именем "John Watson".
     * Вывод на экран курсов, на которые записан(а) Person с именем "Henry Baskerville".
     *
     * @param courseDAOImpl  объект courseDAO
     * @param personDAOImpl  объект personDAO
     * @param subjectDAOImpl объект subjectDAO
     */
    private static void demonstrateWorkWithCourseDAO(CourseDAO courseDAOImpl, PersonDAO personDAOImpl, SubjectDAO subjectDAOImpl) {
        int idHolmes = 0;
        int idWatson = 0;
        int idBaskerville = 0;
        int idJava = 0;
        int idPython = 0;

        for (Person person : personDAOImpl.getAllPersons()) {
            if ("Sherlock Holmes".equals(person.getName())) {
                idHolmes = person.getId();
            }
            if ("John Watson".equals(person.getName())) {
                idWatson = person.getId();
            }
            if ("Henry Baskerville".equals(person.getName())) {
                idBaskerville = person.getId();
            }
        }

        for (Subject subject : subjectDAOImpl.getAllSubjects()) {
            if ("Java".equals(subject.getDescription())) {
                idJava = subject.getId();
            }
            if ("Python".equals(subject.getDescription())) {
                idPython = subject.getId();
            }
        }

        courseDAOImpl.linkPersonWithCourse(personDAOImpl.getById(idHolmes), subjectDAOImpl.getById(idJava));
        courseDAOImpl.linkPersonWithCourse(personDAOImpl.getById(idHolmes), subjectDAOImpl.getById(idPython));
        courseDAOImpl.linkPersonWithCourse(personDAOImpl.getById(idWatson), subjectDAOImpl.getById(idJava));
        courseDAOImpl.linkPersonWithCourse(personDAOImpl.getById(idBaskerville), subjectDAOImpl.getById(idPython));
        System.out.println("Участники Java-курса: " + courseDAOImpl.getPersonsBySubject(subjectDAOImpl.getById(idJava)));
        System.out.println("Участники Python-курса: " + courseDAOImpl.getPersonsBySubject(subjectDAOImpl.getById(idPython)));
        System.out.println("Курсы Мистера Холмса: " + courseDAOImpl.getSubjectsByPerson(personDAOImpl.getById(idHolmes)));
        System.out.println("Курсы Доктора Ватсона: " + courseDAOImpl.getSubjectsByPerson(personDAOImpl.getById(idWatson)));
        System.out.println("Курсы Графа Баскервилля: " + courseDAOImpl.getSubjectsByPerson(personDAOImpl.getById(idBaskerville)));

    }

    /**
     * Создание person.
     * Демонстрация работоспособности методов setName, setBirthDay.
     *
     * @param name имя для создаваемого Person
     * @return объект Person
     */
    private static Person createPerson(String name) {
        Person person = new Person();
        person.setName(name);
        person.setBirthDay(System.currentTimeMillis());
        LOGGER.info("Создан пользователь с именем - {}", name);
        return person;
    }

    /**
     * Создание subject.
     * Демонстрация работоспособности метода setDescription.
     *
     * @param description описание для создаваемого Subject
     * @return объект Subject
     */
    private static Subject createSubject(String description) {
        Subject subject = new Subject();
        subject.setDescription(description);
        LOGGER.info("Создан предмет с описанием - {}", description);
        return subject;
    }
}
