package ru.inno.homeworklesson02;

import java.util.*;

/**
 * Домашнее задание после 2-ой лекции
 */
public class HomeWorkLesson02 {
    public static void main(String[] args) {
        int arrLength = 0;
        boolean check = true;
        Scanner input = new Scanner(System.in);
        System.out.println("Демонстрация работоспособности с консольным вводом");
        System.out.println("Введите размер массива: ");
        while (check) {
            if (input.hasNextInt()) {
                arrLength = input.nextInt();
                check = false;
            } else {
                System.out.println("Некорректный ввод! Введите число: ");
                input.next();
            }
        }


        Integer[] arr = new Integer[arrLength];
        Random rndElem = new Random();
        for (int i = 0; i < arrLength; i++) {
            arr[i] = rndElem.nextInt(1000);
        }

        /* Result of BubbleSort */
        Integer[] resultOfBubbleSort = Sorting.bubbleSort(arr);
        System.out.println("Bubble Sort:");
        for (Integer elem : resultOfBubbleSort) {
            System.out.print(elem + ";");
        }
        System.out.println();
        /* Result of SelectionSort */
        Integer[] resultOfSelectionSort = Sorting.selectionSort(arr);
        System.out.println("Selection Sort:");
        for (Integer elem : resultOfSelectionSort) {
            System.out.print(elem + ";");
        }
        System.out.println();
        /* Result of MergeSort */
        Integer[] resultOfMergeSort = Sorting.mergeSort(arr);
        System.out.println("Merge Sort:");
        for (Integer elem : resultOfMergeSort) {
            System.out.print(elem + ";");
        }

        System.out.println("\nДемонстрация работы с try / catch");

        Integer[] arr1 = {1,6,3,2,15,4};
        Integer[] arr2 = null;
        Integer[] arr3 = {1,6,3,null,15,4};

        System.out.println("\nBubble Sort - good example:");
        callBubbleSort(arr1);
        System.out.println("Bubble Sort - bad example #1:");
        callBubbleSort(arr2);
        System.out.println("Bubble Sort - bad example #2:");
        callBubbleSort(arr3);

        Integer[] arr4 = {2,11,4,6,19,1};
        Integer[] arr5 = null;
        Integer[] arr6 = {2,null,4,6,null,1};

        System.out.println("\nSelection Sort - good example:");
        callSelectionSort(arr4);
        System.out.println("Selection Sort - bad example #1:");
        callSelectionSort(arr5);
        System.out.println("Selection Sort - bad example #2:");
        callSelectionSort(arr6);

        Integer[] arr7 = {21,14,18,990,15,6};
        Integer[] arr8 = null;
        Integer[] arr9 = {2,null,4,null,null,1};

        System.out.println("\nMerge Sort - good example:");
        callMergeSort(arr7);
        System.out.println("Merge Sort - bad example #1:");
        callMergeSort(arr8);
        System.out.println("Merge Sort - bad example #2:");
        callMergeSort(arr9);
    }

    /**
     * Запуск Bubble Sort
     * @param arr массив предназначенный для сортировки
     */
    private static void callBubbleSort(Integer[] arr){
        try {
            Integer[] resultOfBubbleSort = Sorting.bubbleSort(arr);
            System.out.println("Result of Bubble Sort:");
            for (Integer elem : resultOfBubbleSort) {
                System.out.println(elem);
            }
        } catch (NullPointerException ex) {
            System.out.println("Exception! Array is null or array has null element(s).");
        }
    }

    /**
     * Запуск Selection Sort
     * @param arr массив предназначенный для сортировки
     */
    private static void callSelectionSort(Integer[] arr){
        try {
            Integer[] resultOfSelectionSort = Sorting.selectionSort(arr);
            System.out.println("Result of Selection Sort:");
            for (Integer elem : resultOfSelectionSort) {
                System.out.println(elem);
            }
        } catch (NullPointerException ex) {
            System.out.println("Exception! Array is null or array has null element(s).");
        }
    }

    /**
     * Запуск Merge Sort
     * @param arr массив предназначенный для сортировки
     */
    private static void callMergeSort(Integer[] arr){
        try {
            Integer[] resultOfMergeSort = Sorting.mergeSort(arr);
            System.out.println("Result of Merge Sort:");
            for (Integer elem : resultOfMergeSort) {
                System.out.println(elem);
            }
        } catch (NullPointerException ex) {
            System.out.println("Exception! Array is null or array has null element(s).");
        }
    }
}

/**
 * Класс содержащий реализацию сортировок массива
 */
class Sorting {

    /**
     * Сортировка пузырьком
     * @param arr массив для сортировки
     * @return отсортированный массив
     */
    static Integer[] bubbleSort(Integer[] arr) {
        checkArrForNull(arr);
        Integer temp;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        return arr;
    }

    /**
     * Сортировка выбором
     * @param arr массив для сортировки
     * @return отсортированный массив
     */
    static Integer[] selectionSort(Integer[] arr) {
        checkArrForNull(arr);
        Integer temp;
        int min;
        for (int i = 0; i < arr.length - 1; i++) {
            min = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[min]) {
                    min = j;
                }
            }
            temp = arr[i];
            arr[i] = arr[min];
            arr[min] = temp;
        }
        return arr;
    }

    /**
     * Сортировка слиянием
     * @param arr массив предназначенный для сортировки
     * @return отсортированный массив
     */
     static Integer[] mergeSort(Integer[] arr) {
        checkArrForNull(arr);
        if (arr.length < 2) {
            return arr;
        }
        int middle = arr.length / 2;
        Integer[] leftArr = Arrays.copyOfRange(arr, 0, middle);
        Integer[] rightArr = Arrays.copyOfRange(arr, middle, arr.length);
        return merge(mergeSort(leftArr), mergeSort(rightArr));
    }

    /**
     * Слияние двух массивов в один отсортированный
     * @param leftArr первый массив предназначенный для слияния
     * @param rightArr второй массив предназначенный для слияния
     * @return отсортированый массив
     */
    private static Integer[] merge(Integer[] leftArr, Integer[] rightArr) {
        int arrLength = leftArr.length + rightArr.length;
        Integer[] arr = new Integer[arrLength];
        int indexForLeftArr = 0;
        int indexForRightArr = 0;
        for (int i = 0; i < arrLength; i++) {
            if (indexForLeftArr == leftArr.length) {
                arr[i] = rightArr[indexForRightArr++];
            } else if (indexForRightArr == rightArr.length) {
                arr[i] = leftArr[indexForLeftArr++];
            } else {
                if (leftArr[indexForLeftArr] < rightArr[indexForRightArr]) {
                    arr[i] = leftArr[indexForLeftArr++];
                } else {
                    arr[i] = rightArr[indexForRightArr++];
                }
            }
        }
        return arr;
    }

    /**
     * Проверка, явялется ли объект null
     * @param o объект над которым осуществляется проверка;
     */
    private static boolean isNull(Object o){
       return (o == null);
    }

    /**
     * Проверка на null передаваемого массива
     * Проверка наличия елемента массива являющегося null
     * В случае, если передаваемый массив будет являться null, будет вызван NullPointerException
     * В случае наличия в массиве null-элемента будет вызван NullPointerException
     * @param arr массив над которым будет осуществляться проверка
     */
    private static void checkArrForNull(Integer[] arr) throws NullPointerException{
        if (isNull(arr)) {
            throw new NullPointerException();
        }
        for (Integer elem : arr) {
            if (isNull(elem)) {
                throw new NullPointerException();
            }
        }
    }
}